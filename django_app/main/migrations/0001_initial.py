# Generated by Django 5.0.3 on 2024-03-30 16:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Chambers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(verbose_name='URL-адрес камеры')),
                ('title', models.CharField(verbose_name='Название камеры')),
                ('chamber_type', models.CharField(verbose_name='Тип камеры')),
            ],
        ),
    ]
