from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="home"),
    path('ajax/get_updates', views.get_updates, name= "get_updates"),
    path('monitor', views.monitor_page, name="monitor_page")
]
