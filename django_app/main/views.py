from django.shortcuts import render
from django.http import JsonResponse
from .models import TesseractData
import psycopg2

# Create your views here.
def index(request):
    return render(request, "main/index.html")

def get_updates(request):
    try:
        queryset = TesseractData.objects.using("remote").order_by("chamber_name").all()
        return JsonResponse({"chambers":list(queryset.values())})
    except Exception as er:
        print(er)

def monitor_page(request):
    chambers = TesseractData.objects.order_by("chamber_name")
    return render(request, "main/monitor_page.html", {"chambers" :chambers})