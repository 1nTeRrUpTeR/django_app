from django.db import models


class TesseractData(models.Model):
    chamber_name = models.CharField("Название камеры", max_length=50)
    chamber_type = models.CharField("Тип камеры", max_length=50)
    chamber_status = models.CharField("Состояние камеры", max_length=50)
    date = models.CharField("Дата", max_length=50)
    time = models.CharField("Время", max_length=50)
    current_vacuum = models.DecimalField("Текущее давление", default='', max_digits=10, decimal_places=2)
    set_vacuum = models.DecimalField("Установленное давление", default='',max_digits=10, decimal_places=2)
    current_temperature = models.DecimalField("Текущая температура", default='',max_digits=10, decimal_places=2)
    set_temperature = models.DecimalField("Установленная температура", default='',max_digits=10, decimal_places=2)
    fore_pump = models.CharField("Форвакуумный насос", max_length=50)
    turbo_pump = models.CharField("Насос", max_length=50)
    temp_regul = models.CharField("Регулятор температуры", max_length=50)
    pres_regul = models.CharField("Регулятор давления", max_length=50)


    def __str__(self) -> str:
        return self.chamber_name
    
    class Meta:
        db_table = "tesseract_data"
        verbose_name = "Камера"
        verbose_name_plural = "Камеры"