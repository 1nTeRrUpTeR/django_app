from django.urls import path
from . import views

urlpatterns = [
    path('100', views.chamber_100, name="100"),
    path('201', views.chamber_201, name="201"),
    path('203', views.chamber_203, name="203"),
    path('205', views.chamber_205, name="205"),
    path('208', views.chamber_208, name="208"),
    path('212', views.chamber_212, name="212"),
]
