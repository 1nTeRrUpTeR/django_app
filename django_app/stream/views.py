from django.shortcuts import render

# Create your views here.
def chamber_100(request):
    return render(request, "stream/100.html")

def chamber_201(request):
    return render(request, "stream/201.html")

def chamber_203(request):
    return render(request, "stream/203.html")

def chamber_205(request):
    return render(request, "stream/205.html")

def chamber_208(request):
    return render(request, "stream/208.html")

def chamber_212(request):
    return render(request, "stream/212.html")
